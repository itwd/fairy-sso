/**
 *  fairy 版权所有 Copyright@2016年6月21日
 */
package com.fairy.sso.core;

import com.fairy.sso.entity.SSOUserEntity;

/**
 * [SSOResult.java]
 *
 * @ProjectName: [sso-server]
 * @Author: [waysun]
 * @CreateDate: [2016年6月21日 上午10:53:59]
 * @Update: [说明本次修改内容] BY [waysun] [2016年6月21日 上午10:53:59]
 * @Version: [v1.0]
 */

public class SSOResult {

	private String code;
	private String msg;
	
	private SSOUserEntity ssoUserEntity;

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public SSOUserEntity getSsoUserEntity() {
		return ssoUserEntity;
	}

	public void setSsoUserEntity(SSOUserEntity ssoUserEntity) {
		this.ssoUserEntity = ssoUserEntity;
	}


	
}



