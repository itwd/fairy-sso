package com.fairy.sso.core;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.log4j.Logger;

public class HttpUtil {
	private static Logger log = Logger.getLogger(HttpUtil.class);

	public static String post( String sso_url, String param, String param_value) 
	{
		 String str="";
		long start = System.currentTimeMillis();      
		PostMethod postMethod = new PostMethod(sso_url);
		System.out.println("post请求地址:"+sso_url);
		log.info("post请求地址:"+sso_url);
		postMethod.addParameter(param, param_value);
		
		HttpClient httpClient = new HttpClient();
		try {
		    httpClient.executeMethod(postMethod);
		    str = postMethod.getResponseBodyAsString();
		    postMethod.releaseConnection();
		} catch (Exception e) {
		    e.printStackTrace();
		}
		long end = System.currentTimeMillis(); 
		System.out.println("<<===================post请求 运行时间::::::::::::::::::"+(end-start)+"毫秒============================>>"); 
		log.info("<<===================post请求  运行时间:::::::::::::::"+(end-start)+"毫秒============================>>");    
		return str;
	}
}
