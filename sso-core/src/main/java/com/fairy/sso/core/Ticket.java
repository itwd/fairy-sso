/**
 * 版权所有 Copyright@@2016-03-18 10:39:05
 */
package com.fairy.sso.core;

import java.io.Serializable;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * [SSOCache.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-18 10:39:05]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-03-18 10:39:05]
 * @Version: [v1.0]
 */
public class Ticket implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	
	/* 用户Id */
	private String userId;
	
	/* 用户账号 (用户名/手机号/邮箱)*/
	private String accountNo;

	/* 用户IP */
	private String ip;

	/* 接入单点的客户端应用集群ip,主要是用于解决通过ip访问时应用做集群时不能获得ticket问题(也可用于跨域) */
	//private String clusterIp;
	
	/* 登录的应用名称,英文,http://127.0.0.1/xx中的xx */
	private String appName;
	
	/* 应用部署的ip地址 */
	private String appIp;

	/* ticket唯一标识 */
	private String ticketKey;
	
	/* Ticket 生成时间*/
	private long createTime = System.currentTimeMillis();


	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	public long getCreateTime() {
		return createTime;
	}

	public void setCreateTime(long time) {
		this.createTime = time;
	}
	
	public String getAccountNo() {
		return accountNo;
	}

	public void setAccountNo(String accountNo) {
		this.accountNo = accountNo;
	}
	

	public String getTicketKey() {
		return ticketKey;
	}

	public void setTicketKey(String ticketKey) {
		this.ticketKey = ticketKey;
	}

	public String getAppName() {
		return appName;
	}

	public void setAppName(String appName) {
		this.appName = appName;
	}

	public String getAppIp() {
		return appIp;
	}

	public void setAppIp(String appIp) {
		this.appIp = appIp;
	}

	public String toString()
	{
		ObjectMapper mapper = new ObjectMapper();  
	    String json="";
		try {
			json = mapper.writeValueAsString(this);
		} catch (JsonProcessingException e) {
			e.printStackTrace();
		}
		//String str="[sysName:"+this.getSysName()+",userId:"+this.getUserId()+",ip:"+this.getIp()+",time:+"+this.getTime()+"]";
		return json;
	}



}
