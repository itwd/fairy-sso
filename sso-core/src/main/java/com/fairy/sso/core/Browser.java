/**
 * Copyright (c) 2011-2014, hubin (jobob@qq.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
/**
 * 版权所有 Copyright@yyk1504@163.com
 */
package com.fairy.sso.core;



import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.fairy.core.util.MD5Util;

/**
 * [Browser.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-3-25 下午1:46:15]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-3-25 下午1:46:15]
 * @Version: [v1.0]
 */
public class Browser {

	//private static final Logger logger = Logger.getLogger("Browser");
	private static Logger logger = Logger.getLogger(CookieHelper.class);
	/**
	 * 混淆浏览器版本信息
	 * 
	 * @Description 获取浏览器客户端信息签名值
	 * @param request
	 * @return
	 */
	public static String getUserAgent(HttpServletRequest request, String value) {
		StringBuffer sf = new StringBuffer();
		sf.append(value);
		sf.append("-");
		sf.append(request.getHeader("user-agent"));

		/**
		 * MD5 浏览器版本信息
		 */
		logger.info("Browser info:" + sf.toString());
		return MD5Util.GetMD5Code(sf.toString());
	}

	/**
	 * <p>
	 * 请求浏览器是否合法 (只校验客户端信息不校验domain)
	 * </p>
	 * @param request
	 * @param userAgent
	 *            浏览器客户端信息
	 * @return
	 */
	public static boolean isLegalUserAgent(HttpServletRequest request, String value, String userAgent) {
		String rlt = getUserAgent(request, value);

		if (rlt.equalsIgnoreCase(userAgent)) {
			logger.info("Browser isLegalUserAgent is legal. Browser getUserAgent:" + rlt);
			return true;
		}

		logger.info("Browser isLegalUserAgent is illegal. Browser getUserAgent:" + rlt);
		return false;
	}
	
}
