/**
 *  fairy 版权所有 Copyright@2016年6月8日
 */
package com.fairy.sso.core;

/**
 * [TicketHelp.java]
 *
 * @ProjectName: [sso-pro]
 * @Author: [waysun]
 * @CreateDate: [2016年6月8日 上午9:38:02]
 * @Update: [说明本次修改内容] BY [waysun] [2016年6月8日 上午9:38:02]
 * @Version: [v1.0]
 */

public class TicketHelp {

	public String ticket;

	public String getTicket() {
		return ticket;
	}

	public void setTicket(String ticket) {
		this.ticket = ticket;
	}
	
}



