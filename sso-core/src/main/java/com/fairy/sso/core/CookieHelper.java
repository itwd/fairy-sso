/**
 * 版权所有 Copyright@yyk1504@163.com
 */
package com.fairy.sso.core;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;


/**
 * [LoginController.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-3-25 下午1:46:15]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-3-25 下午1:46:15]
 * @Version: [v1.0]
 */
public class CookieHelper {

    private static Logger log = Logger.getLogger(CookieHelper.class);
	
    /**
     * 
     * @param response
     * @param domain
     * @param path
     * @param name
     * @param value
     * @param maxAge:单位
     */
	public static void addCookie(HttpServletResponse response, String domain, String path, String name, String value,int maxAge) {
		Cookie cookie = new Cookie(name, value);
		/**
		 * 不设置该参数默认 当前所在域
		 */
		if (domain != null && !"".equals(domain)) {
			cookie.setDomain(domain);
		}
		if(null==path||"".equals(path))
		{
			path="/";
		}
		log.info("设置cookie,path:"+path+",maxAge:"+maxAge+",domain:"+domain);
		cookie.setPath(path);
		cookie.setMaxAge(maxAge);
		response.addCookie(cookie);


	}
	public static void removeCookie(HttpServletRequest request,  HttpServletResponse response, String name) 
	{
		    if (null == name)
		    {
		    	log.info("cookie name is  null");
		        return;
		    }
		    Cookie cookie = getCookie(request, name);
		    if(null != cookie)
		    {
			      cookie.setPath("/");
			      cookie.setValue(null);
			      cookie.setMaxAge(0);
			      log.info("cookies is  remove");
			      response.addCookie(cookie);
		    }
    }

	  /**
	   * 根据Cookie名称得到Cookie对象，不存在该对象则返回Null
	   * 
	   * @param request
	   * @param name
	   * @return
	   */
	  public static Cookie getCookie(HttpServletRequest request, String name) 
	  {
	    Cookie[] cookies = request.getCookies();
	    if (null == cookies || null == name || name.length() == 0) 
	    {
	    	 log.info("cookies is  null");
	         return null;
	    }
	    Cookie cookie = null;
	    for (Cookie c : cookies) 
	    {
		     if (name.equals(c.getName())) 
		     {
			       cookie = c;
			       break;
		     }
	    }
	    return cookie;
	  }

	  public static String getCookieValueByName(HttpServletRequest request, String name) 
	  {
	       Cookie[] cookies = request.getCookies();
	        String cookieValue = "";
	        if (null != cookies) 
	        {
	            for (Cookie cookie : cookies) 
	            {
	                if (name.equals(cookie.getName())) {
	                    cookieValue = cookie.getValue();
	                    log.info("get cookie value by name :"+cookieValue);
	                    break;
	                }
	            }
	        }

	    return cookieValue;
	  }

	/**
	 * 
	 * <p> 防止伪造SESSIONID攻击. 用户登录校验成功销毁当前JSESSIONID. 创建可信的JSESSIONID </p>
	 * @param request 当前HTTP请求
	 * @param value  用户ID等唯一信息
	 */
	public static void authJSESSIONID(HttpServletRequest request, String value) {
		request.getSession().invalidate();
		request.getSession().setAttribute("SSO-" + value, true);
	}

	
}
