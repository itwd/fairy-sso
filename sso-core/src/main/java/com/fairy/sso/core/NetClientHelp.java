/**
 *  fairy 版权所有 Copyright@2016年6月7日
 */
package com.fairy.sso.core;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ResourceBundle;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

/**
 * [NetClientHelp.java]
 *
 * @ProjectName: [sso-pro]
 * @Author: [waysun]
 * @CreateDate: [2016年6月7日 下午2:43:33]
 * @Update: [说明本次修改内容] BY [waysun] [2016年6月7日 下午2:43:33]
 * @Version: [v1.0]
 */

public class NetClientHelp {
	private static Logger log = Logger.getLogger(NetClientHelp.class);
	
	public String newTicket(String userId,String userIp) 
	{
		String ticket=null;
		try{
	        ResourceBundle resource = ResourceBundle.getBundle("sso");
	        String  sso_sync_ticket_url=resource.getString("sso.sync.ticket.url");
	        String ticketUrl=sso_sync_ticket_url+"/createTicket.do";
	        
	        String param="token";
	        String param_value=userId+","+userIp;
	        
	        ticket= HttpUtil.post(ticketUrl, param, param_value);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		
		return ticket;
	}
	
	public String getTicket(HttpServletRequest request) 
	{
		Cookie[] cookies = request.getCookies();
		String cookieValue = "";
		if (null != cookies) 
		{
		    for (Cookie cookie : cookies) 
		    {
		        if (SSOConstants.SSO_COOKIE_KEY.equals(cookie.getName())) {
		            cookieValue = cookie.getValue();
		            System.out.println(this.getClass().getName()+",<<<<<==================>>>>>应用端从cookies取值,cookieValue:"+cookieValue);
		            log.info(this.getClass().getName()+",<<<<<==================>>>>>应用端从cookies取值,cookieValue:"+cookieValue);
		            break;
		        }
		    }
		}

		
		return cookieValue;
	}
	public static String getIpAddr(HttpServletRequest request) {   
	     String ipAddress = null;   
	     //ipAddress = this.getRequest().getRemoteAddr();   
	     ipAddress = request.getHeader("x-forwarded-for");   
	     if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {   
	      ipAddress = request.getHeader("Proxy-Client-IP");   
	     }   
	     if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {   
	         ipAddress =request.getHeader("WL-Proxy-Client-IP");   
	     }   
	     if(ipAddress == null || ipAddress.length() == 0 || "unknown".equalsIgnoreCase(ipAddress)) {   
	      ipAddress =request.getRemoteAddr();   
	      if(ipAddress.equals("127.0.0.1")){   
	       //根据网卡取本机配置的IP   
	       InetAddress inet=null;   
	    try {   
	     inet = InetAddress.getLocalHost();   
	    } catch (UnknownHostException e) {   
	     e.printStackTrace();   
	    }   
	    ipAddress= inet.getHostAddress();   
	      }   
	            
	     }   
	  
	     //对于通过多个代理的情况，第一个IP为客户端真实IP,多个IP按照','分割   
	     if(ipAddress!=null && ipAddress.length()>15){ //"***.***.***.***".length() = 15   
	         if(ipAddress.indexOf(",")>0){   
	             ipAddress = ipAddress.substring(0,ipAddress.indexOf(","));   
	         }   
	     }   
	     return ipAddress;    
	  }

	
}



