/**
 *  版权所有 Copyright@2016年4月13日下午2:20:52
 */
package com.fairy.sso.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;

import com.fairy.core.util.MD5Util;
import com.fairy.core.util.UUIDUtil;

/**
 * [SSOHelp.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016年4月13日下午2:20:52]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016年4月13日下午2:20:52]
 * @Version: [v1.0]
 */
public class SSOHelp {
	private static Logger log = Logger.getLogger(SSOHelp.class);

	public static ArrayList<String> getIpList(String url)
	{
		ArrayList<String> al=new ArrayList<String>();
		if(null!=url&&!"".equals(url))
		{
			if(url.contains(","))
			{
				String []ips=url.split("//,");
				for(String ip:ips){
					al.add(ip);
				}
			}
			else
			{
				al.add(url);
			}
		}
		return al;
	}
	
	public static Map<String,String>getTicketIpMap(String ticket,String url)
	{
		Map<String,String> map=new HashMap<String,String>();
		if(null!=url&&!"".equals(url))
		{
			if(url.contains(","))
			{
				String []ips=url.split("//,");
				for(String ip:ips){
					map.put(ip,ticket);
				}
			}
			else
			{
				map.put(url,ticket);
			}
		}
		return map;
	}
	public static Ticket getTicket(String appName,String appIp,String accountNo, String userId, String ip,String ticketKey) 
	{
		Ticket ticket=new Ticket();
		//String clusterIp=fairyConfig.getSsoClientUrl();
		long time = System.currentTimeMillis();
		ticket.setIp(ip);
		ticket.setCreateTime(time);
		ticket.setUserId(userId);
		ticket.setAccountNo(accountNo);
		//ticket.setClusterIp(clusterIp);                        
		ticket.setTicketKey(ticketKey);
		ticket.setAppIp(appIp);
		ticket.setAppName(appName);
		
		log.info("ticket is :"+ticket.toString());
		return ticket;
	}

	/**
	public static String getMD5TicketKey(String ticketSecretkey ) {
		String str_rev="";
		String uuid=UUIDUtil.getUUID();
		byte[] inputData = uuid.getBytes(); 
		try {
			inputData = DESCoder.encrypt(inputData, ticketSecretkey);
			String mi=DESCoder.encryptBASE64(inputData);
			 log.info("加密后::"+mi);
			 byte[] outputData = DESCoder.decrypt(inputData, ticketSecretkey);  
		     String outputStr = new String(outputData);  
		     log.info("解密后::"+outputStr);
		     String str=SHACoder.sha512Encode(mi);
		     log.info("SHA512::"+str);
		     str_rev=MD5Util.GetMD5Code(str);
		     log.info("MD5::"+str_rev);
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		return str_rev;
	}
	*/
	
	public static String getMD5TicketKey( ) {
		String str_rev="";
		String uuid=UUIDUtil.getUUID();
		str_rev=MD5Util.GetMD5Code(uuid);
		log.info("MD5::"+str_rev);
		return str_rev;
	}
}
