package com.fairy.sso.core;

import java.util.ArrayList;
import java.util.List;

public class SSOConstants
{
	  public static int temp = 1;
	  public static String CACHE_TYPE_LOCAL = "local";
	  public static String CACHE_TYPE_REDIS = "redis";
	  public static String CACHE_TYPE_SESSION = "session";
	  public static String CACHE_TYPE_DB = "db";
	  public static String CACHE_TYPE_DUBBO = "dubbo";
	
	  public static String LOGIN_PAGE_SUBMIT_INFO_USERNAME = "userName";
	  public static String LOGIN_PAGE_SUBMIT_INFO_PASSWORD = "passWord";
	
	  public static String CLIENT_URL = "service";
	
	  public static String SSO_COOKIE_KEY = "FAIRY-SSO-KEY";
	
	  public static String SSO_IMG_CODE = "sso-code";
	
	  public static List<String> noCheckUrlList = new ArrayList<String>();
	
	  static { noCheckUrlList.add("/index.do");
	    noCheckUrlList.add("/login.do");
	    noCheckUrlList.add("/logout.do");
	    noCheckUrlList.add("/ticket.do");
	  }
	  

	  public static String SESSION_DATA="userId";
	  //public static String SSO_COOKIE_KEY="FAIRY-SSO-KEY";
	  public static String TICKET="ticket";
	  /***********************************************************************************/		 
	  public static String SSO_COOKIE="sso-cookie";
		 
	  public static String SSO_SERVER_URL="sso_server_url";
	  public static String JUMP_URL="jump_url";
}