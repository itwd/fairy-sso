package com.fairy.sso.exception;

public class SSOException extends Exception {
	private static final long serialVersionUID = 1L;

	public SSOException() 
	{  
		  
		super();  
		  
	}  
		  
	public SSOException(String msg) 
	{  		  
	     super(msg);  		  
	}  
	  
	public SSOException(String msg, Throwable cause) 
	{  
	    super(msg, cause);  
	}  
	  
	public SSOException(Throwable cause) 
	{  
	     super(cause);  
	}  		  
}
