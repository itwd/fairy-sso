/**
 *  版权所有 Copyright@2016年4月11日下午12:02:07
 */
package com.fairy.sso.client.session.impl;

import javax.servlet.http.HttpSession;

import com.fairy.sso.client.session.SSOSession;

/**
 * [SSOSessionImpl.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016年4月11日下午12:02:07]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016年4月11日下午12:02:07]
 * @Version: [v1.0]
 */
public class HttpSessionStorage implements SSOSession{
	//HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();  
	private HttpSession session;//request.getSession();

	public HttpSessionStorage(HttpSession session)
	{
		this.session=session;
	}
	@Override
	public void setData(String key, String t) {
		session.setAttribute(key, t);
	}

	@Override
	public String getData(String key) {
		String rev=(String)session.getAttribute(key);
		return rev;
	}

	@Override
	public boolean deleteData(String key) {
		boolean bl=false;
		session.removeAttribute(key);
		bl=true;
		return bl;
	}

}
