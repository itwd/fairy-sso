/**
 *  版权所有 Copyright@2016年4月11日下午1:58:05
 */
package com.fairy.sso.client;

import java.util.ResourceBundle;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.fairy.sso.client.session.SSOSession;
import com.fairy.sso.client.session.impl.HttpSessionStorage;

/**
 * [SSOSessonFactory.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016年4月11日下午1:58:05]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016年4月11日下午1:58:05]
 * @Version: [v1.0]
 */
public class SSOSessionFactory extends Factory {
	private static Logger log = Logger.getLogger(SSOSessionFactory.class);
	@SuppressWarnings("unchecked")
	public SSOSession  getSSOSession(String sessionType) { 
		log.info("sessionType is:"+sessionType);
		SSOSession session = null; 
	      try { 
	    	  if(sessionType.equals("httpsession"))
	    	  {
	    		  HttpSession httpSession=this.getSession();
	    		  session =new HttpSessionStorage(httpSession);
	    	  }
	    	  else
	    	  if(sessionType.equals("redis"))
	    	  {
	    		  //session=
	    		    ResourceBundle resource = ResourceBundle.getBundle("sso");
	    	        String  session_impl_class=resource.getString("session_impl_class");
	    	        Class<SSOSession> redisSession=null;  
	    	        try{  
	    	            //一般尽量采用这种形式  
	    	        	redisSession=(Class<SSOSession>) Class.forName(session_impl_class);  
	    	        }catch(Exception e){  
	    	            e.printStackTrace();  
	    	        }
	    	        session=redisSession.newInstance();
	    	        log.info("实现类:"+session.getClass().getName());
	    	        //demo2=new Demo().getClass();  
	    	        //demo3=Demo.class; 
	    	  }
	    	  else
	    	  {
	    		  HttpSession httpSession=this.getSession();
	    		  session =new HttpSessionStorage(httpSession);
	    	  }
	    	   
	      } catch (Exception e) { 
	        e.printStackTrace(); 
	      } 
	      log.info("sessionType is:"+sessionType+",session is:"+session.getClass().getName());
	      return session; 
	    } 
}
