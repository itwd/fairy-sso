/**
 *  版权所有 Copyright@2016年4月11日上午11:57:09
 */
package com.fairy.sso.client.session;

/**
 * [SSOSession.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016年4月11日上午11:57:09]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016年4月11日上午11:57:09]
 * @Version: [v1.0]
 */
public interface SSOSession {

	public void setData(String key,String value);
	
	public String getData(String key);
	
	public boolean deleteData(String key);
	
}
