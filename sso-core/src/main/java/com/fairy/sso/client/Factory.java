/**
 *  版权所有 Copyright@2016年4月11日下午1:58:59
 */
package com.fairy.sso.client;

import javax.servlet.http.HttpSession;

import com.fairy.sso.client.session.SSOSession;

/**
 * [Factory.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016年4月11日下午1:58:59]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016年4月11日下午1:58:59]
 * @Version: [v1.0]
 */
public abstract class  Factory {
    
	private HttpSession session;
	
	public abstract SSOSession  getSSOSession(String cacheType);

	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	} 
	
	
}
