/**
 *  版权所有 Copyright@2016-03-31 16:13:56
 */

package com.fairy.sso.entity;

/**   
 * @Title: UserEntity
 * @Description: 个人用户
 * @author yyk1504@163.com
 * @date 2016-03-31 16:13:56
 * @version V1.0   
 *
 */

public class UserEntity implements java.io.Serializable {

    private static final long serialVersionUID = 1L;
    
	/**主键*/
	private java.lang.String userId;
	/**用户名*/
	private java.lang.String userName;
	/**登录密码*/
	private java.lang.String userPass;
	/**邮箱*/
	private java.lang.String email;
	/**手机号*/
	private java.lang.String mobile;
	/**用户身份(0:游客,1:普通注册用户,2:专业用户）*/
	private java.lang.Integer userIdentity;
	/**验证类型(0:普通注册密码1:手机动态密码2:动态令牌3:UK证书)*/
	private java.lang.Integer verifyType;
	/**证件类型*/
	private java.lang.Integer certificateType;
	/**证件号码*/
	private java.lang.String certificateNo;
	/**注册时间*/
	private java.lang.String registerTime;
	/**注册渠道(手机银行\支付平台\个人网银)*/
	private java.lang.String registerChannel;
	/**注册ip*/
	private java.lang.String registerIp;
	/**注册终端(android,iphone,ipad,)*/
	private java.lang.String registerTerminal;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  主键
	 */
	public java.lang.String getUserId(){
		return this.userId;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  主键
	 */
	public void setUserId(java.lang.String userId){
		this.userId = userId;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  用户名
	 */
	public java.lang.String getUserName(){
		return this.userName;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  用户名
	 */
	public void setUserName(java.lang.String userName){
		this.userName = userName;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  登录密码
	 */
	public java.lang.String getUserPass(){
		return this.userPass;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  登录密码
	 */
	public void setUserPass(java.lang.String userPass){
		this.userPass = userPass;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  邮箱
	 */
	public java.lang.String getEmail(){
		return this.email;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  邮箱
	 */
	public void setEmail(java.lang.String email){
		this.email = email;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  手机号
	 */
	public java.lang.String getMobile(){
		return this.mobile;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  手机号
	 */
	public void setMobile(java.lang.String mobile){
		this.mobile = mobile;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  用户身份(0:游客,1:普通注册用户,2:专业用户）
	 */
	public java.lang.Integer getUserIdentity(){
		return this.userIdentity;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  用户身份(0:游客,1:普通注册用户,2:专业用户）
	 */
	public void setUserIdentity(java.lang.Integer userIdentity){
		this.userIdentity = userIdentity;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  验证类型(0:普通注册密码1:手机动态密码2:动态令牌3:UK证书)
	 */
	public java.lang.Integer getVerifyType(){
		return this.verifyType;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  验证类型(0:普通注册密码1:手机动态密码2:动态令牌3:UK证书)
	 */
	public void setVerifyType(java.lang.Integer verifyType){
		this.verifyType = verifyType;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  证件类型
	 */
	public java.lang.Integer getCertificateType(){
		return this.certificateType;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  证件类型
	 */
	public void setCertificateType(java.lang.Integer certificateType){
		this.certificateType = certificateType;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  证件号码
	 */
	public java.lang.String getCertificateNo(){
		return this.certificateNo;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  证件号码
	 */
	public void setCertificateNo(java.lang.String certificateNo){
		this.certificateNo = certificateNo;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  注册时间
	 */
	public java.lang.String getRegisterTime(){
		return this.registerTime;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  注册时间
	 */
	public void setRegisterTime(java.lang.String registerTime){
		this.registerTime = registerTime;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  注册渠道(手机银行\支付平台\个人网银)
	 */
	public java.lang.String getRegisterChannel(){
		return this.registerChannel;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  注册渠道(手机银行\支付平台\个人网银)
	 */
	public void setRegisterChannel(java.lang.String registerChannel){
		this.registerChannel = registerChannel;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  注册ip
	 */
	public java.lang.String getRegisterIp(){
		return this.registerIp;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  注册ip
	 */
	public void setRegisterIp(java.lang.String registerIp){
		this.registerIp = registerIp;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  注册终端(android,iphone,ipad,)
	 */
	public java.lang.String getRegisterTerminal(){
		return this.registerTerminal;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  注册终端(android,iphone,ipad,)
	 */
	public void setRegisterTerminal(java.lang.String registerTerminal){
		this.registerTerminal = registerTerminal;
	}
}
