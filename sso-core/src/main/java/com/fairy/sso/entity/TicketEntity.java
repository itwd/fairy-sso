/**
 *  版权所有 Copyright@2016-03-31 16:44:46
 */

package com.fairy.sso.entity;

import java.io.Serializable;


/**   
 * @Title: TicketEntity
 * @Description: 票据
 * @author yyk1504@163.com
 * @date 2016-03-31 16:44:46
 * @version V1.0   
 *
 */

public class TicketEntity implements Serializable {

    private static final long serialVersionUID = 1L;
    
	/**key*/
	private java.lang.String key;
	/**ticket*/
	private java.lang.String ticket;
	/**创建时间*/
	private java.lang.String createTime;
	/**是否有效（0：有效，1：失效）*/
	private java.lang.Integer enable;
	
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  key
	 */
	public java.lang.String getKey(){
		return this.key;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  key
	 */
	public void setKey(java.lang.String key){
		this.key = key;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  ticket
	 */
	public java.lang.String getTicket(){
		return this.ticket;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  ticket
	 */
	public void setTicket(java.lang.String ticket){
		this.ticket = ticket;
	}
	/**
	 *方法: 取得java.lang.String
	 *@return: java.lang.String  创建时间
	 */
	public java.lang.String getCreateTime(){
		return this.createTime;
	}

	/**
	 *方法: 设置java.lang.String
	 *@param: java.lang.String  创建时间
	 */
	public void setCreateTime(java.lang.String createTime){
		this.createTime = createTime;
	}
	/**
	 *方法: 取得java.lang.Integer
	 *@return: java.lang.Integer  是否有效（0：有效，1：失效）
	 */
	public java.lang.Integer getEnable(){
		return this.enable;
	}

	/**
	 *方法: 设置java.lang.Integer
	 *@param: java.lang.Integer  是否有效（0：有效，1：失效）
	 */
	public void setEnable(java.lang.Integer enable){
		this.enable = enable;
	}
}
