/**
 *  版权所有 Copyright@2016年3月31日上午10:29:48
 */
package com.fairy.sso.entity;

import java.io.Serializable;

/**
 * [SSOUserEntity.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016年3月31日上午10:29:48]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016年3月31日上午10:29:48]
 * @Version: [v1.0]
 */
public class SSOUserEntity implements Serializable{

	private static final long serialVersionUID = 1L;
	
	private String account;
	private String userId;
	private String userName;
	public String getAccount() {
		return account;
	}
	public void setAccount(String account) {
		this.account = account;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
}
