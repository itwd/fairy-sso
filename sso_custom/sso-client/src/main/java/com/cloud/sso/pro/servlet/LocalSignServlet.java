package com.cloud.sso.pro.servlet;

import java.io.IOException;
import java.net.UnknownHostException;
import java.util.Map;
import java.util.ResourceBundle;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.fairy.sso.client.Factory;
import com.fairy.sso.client.SSOSessionFactory;
import com.fairy.sso.client.local.login.ClientLocalLoginService;
import com.fairy.sso.client.local.login.impl.ClientLocalLoginServiceImpl;
import com.fairy.sso.client.service.SSOService;
import com.fairy.sso.client.service.impl.SSOServiceImpl;
import com.fairy.sso.client.session.SSOSession;
import com.fairy.sso.core.SSOConstants;

public class LocalSignServlet extends HttpServlet {
    private static final long serialVersionUID = -1334093914490423930L;
    Logger log = Logger.getLogger(LocalSignServlet.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      	 exe(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException 
    {
     	 exe(request, response);
    }

	private void exe(HttpServletRequest request, HttpServletResponse response)
			throws UnknownHostException, ServletException, IOException {
		
		HttpSession httpSession = request.getSession();


        String userName=request.getParameter("userName");
        String passWord=request.getParameter("passWord");
        if(null!=userName&&!"".equals(userName)&&null!=passWord&&!"".equals(passWord))
        {
        	ResourceBundle resource = ResourceBundle.getBundle("sso");
        	String  sessionType=resource.getString("session_type");
            Factory factory = new SSOSessionFactory();
            factory.setSession(httpSession);
   
        	SSOSession session = factory.getSSOSession(sessionType); 
        	String accountNo =session.getData(SSOConstants.SESSION_DATA);
        	String userId="";
        	if(null!=accountNo&&!"".equals(accountNo))
        	{
        		userId=accountNo;
        		/**********************************************集成到登录逻辑**********************************************/
            	//此段代码需要集成到登录页面在本地的登录逻辑中(登录成功获得userId后再调用此段代码)
            	ClientLocalLoginService localLogin=new ClientLocalLoginServiceImpl();
        		boolean bl=localLogin.optLocalLogin(request, response, userId);
        		/**********************************************集成到登录逻辑**********************************************/
        		request.getRequestDispatcher("/WEB-INF/jsp/welcome.jsp").forward(request, response);
        	}
        	else
        	{
                /***********************************************/
                //登录逻辑
                userId="TEST_0000_ANXT_O9MN";
                SSOService ssoService=new SSOServiceImpl();
                Map<String,Object>map=ssoService.sign(accountNo, passWord);
                userId=map.get("userId").toString();
                /***********************************************/
                if(null==userId||"".equals(userId))
                {
                	//用户不存在重新登录
                	request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
                }
                else
                {
            		/**********************************************集成到登录逻辑**********************************************/
                	//此段代码需要集成到登录页面在本地的登录逻辑中(登录成功获得userId后再调用此段代码)
                	ClientLocalLoginService localLogin=new ClientLocalLoginServiceImpl();
            		boolean bl=localLogin.optLocalLogin(request, response, userId);
            		/**********************************************集成到登录逻辑**********************************************/
                	request.getRequestDispatcher("/WEB-INF/jsp/welcome.jsp").forward(request, response);
                }
        	}


        }
        else
        {
        	//用户不存在重新登录
        	request.getRequestDispatcher("/WEB-INF/jsp/login.jsp").forward(request, response);
        }


        
	}
}