package com.fairy.sso.client.local.login.impl;

import java.net.InetAddress;
import java.util.ResourceBundle;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.fairy.sso.client.Factory;
import com.fairy.sso.client.SSOSessionFactory;
import com.fairy.sso.client.local.login.ClientLocalLoginService;
import com.fairy.sso.client.session.SSOSession;
import com.fairy.sso.core.CookieHelper;
import com.fairy.sso.core.NetClientHelp;
import com.fairy.sso.core.NetHelp;
import com.fairy.sso.core.SSOConstants;

public class ClientLocalLoginServiceImpl implements ClientLocalLoginService{
	 Logger log = Logger.getLogger(ClientLocalLoginServiceImpl.class);
	public boolean optLocalLogin(HttpServletRequest request,HttpServletResponse response,String userId){
		boolean bl=false;
		try
		{
			HttpSession httpSession = request.getSession();
			Factory factory = new SSOSessionFactory();
	    	factory.setSession(httpSession);
	        
	        NetClientHelp nc=new NetClientHelp();
	        ResourceBundle resource = ResourceBundle.getBundle("sso");
	        String  sessionType=resource.getString("session_type");
	    	SSOSession session = factory.getSSOSession(sessionType); 
	        String userIp=NetClientHelp.getIpAddr(request);
	        //String userId=session.getData(SSOConstants.SESSION_DATA);
	        
	        if(null!=userId&&!userId.equals(""))
	        {
	        	//userId="TEST_0000_ANXT_O9MN";
	            String ticket=nc.newTicket(userId, userIp);
	      		System.out.println("<-------------------   创建ticket返回结果::::"+ticket+"------------------------->");
	    		log.info("<-------------------   创建ticket返回结果::::"+ticket+"------------------------->");
	    		if(null!=ticket&&!"".equals(ticket))
	    		{
	    			String domain=  NetHelp.getDomainOrIP(request);
	    			boolean isIp=NetHelp.isIP(domain);
	        		if(!isIp)
	        		{
	            		System.out.println("<-------------------   domain is not ip,"+domain+"------------------------->");
	            		log.info("<-------------------   domain is not ip,"+domain+"------------------------->");
	        			domain="."+domain;
	        		}
	        		else
	        		{
	        	        //本机ip
	        			InetAddress addr=null;
						addr = InetAddress.getLocalHost();
	        	        String ssoAppCientIp=addr.getHostAddress().toString();//获得本机IP
	        	        
	        	       
	        			System.out.println("<=====================================netpay 配置为130时1.1.0版本获得的 本机ip:::::::"+ssoAppCientIp+"=====================================>");
	        			log.info("<=====================================netpay 配置为130时1.1.0版本获得的本机ip:::::::"+ssoAppCientIp+"=====================================>");
	        	        ssoAppCientIp=NetHelp.getLocalHostIp();
	        	 
	        			System.out.println("<=====================================netpay 配置为130时1.1.1版本获得的 本机ip:::::::"+ssoAppCientIp+"=====================================>");
	        			log.info("<=====================================netpay 配置为130时1.1.1版本获得的 本机ip:::::::"+ssoAppCientIp+"=====================================>");
	        	        
	        	        
	        			domain=ssoAppCientIp;
	            		System.out.println("<-------------------   domain is  ip"+domain+"------------------------->");
	            		log.info("<-------------------   domain is  ip"+domain+"------------------------->");
	        		}
	    			 //创建cookie
	    			CookieHelper.addCookie(response, domain, "/", SSOConstants.SSO_COOKIE_KEY,ticket, -1);
	    			session.setData(SSOConstants.SESSION_DATA,userId);
	    			bl=true;
	    		}
	        }
	
			
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		return bl;

	}
}
