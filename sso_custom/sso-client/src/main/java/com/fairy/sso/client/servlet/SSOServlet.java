/**
 *  fairy 版权所有 Copyright@2016年6月13日
 */
package com.fairy.sso.client.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fairy.sso.core.CookieHelper;
import com.fairy.sso.core.NetClientHelp;
import com.fairy.sso.core.NetHelp;
import com.fairy.sso.core.SSOConstants;

/**
 * [SSOServlet.java]
 *
 * @ProjectName: [sso-pro]
 * @Author: [waysun]
 * @CreateDate: [2016年6月13日 下午4:57:30]
 * @Update: [说明本次修改内容] BY [waysun] [2016年6月13日 下午4:57:30]
 * @Version: [v1.0]
 */

public class SSOServlet extends HttpServlet {
    private static final long serialVersionUID = -1334093914490423930L;
    Logger log = Logger.getLogger(SSOServlet.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String ssoCookie=request.getParameter(SSOConstants.TICKET);
		System.out.println(this.getClass().getName()+",:::::::::::::::::::ssoCookie:"+ssoCookie);
		log.info(this.getClass().getName()+",:::::::::::::::::::ssoCookie:"+ssoCookie);
    	try{
        	if(null!=ssoCookie)
        	{
    			String domain=  NetHelp.getDomainOrIP(request);//NetHelp.getDomain(request);
        		System.out.println(this.getClass().getName()+"-------------------domain-------------------------"+domain);
        		log.info(this.getClass().getName()+"-------------------domain-------------------------"+domain);

        		
        		NetClientHelp nc=new NetClientHelp();
                String ticket =nc.getTicket(request);
                if(null!=ticket&&!"".equals(ticket))
                {
                    if(ssoCookie.equals(ticket))
                    {
            			CookieHelper.removeCookie(request, response, SSOConstants.SSO_COOKIE_KEY);
            			
                		System.out.println(this.getClass().getName()+"<++++++++++++++++++++++++++++++++++"+domain+"   删除cookie成功++++++++++++++++++++++++++++++++++++>");
                		log.info(this.getClass().getName()+"<++++++++++++++++++++++++++++++++++"+domain+"   删除cookie成功++++++++++++++++++++++++++++++++++++>");	
                    }


                }

        	}
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  
    }

}


