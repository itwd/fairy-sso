package com.fairy.sso.client.servlet;

import java.io.IOException;
import java.net.InetAddress;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.fairy.sso.core.CookieHelper;
import com.fairy.sso.core.NetClientHelp;
import com.fairy.sso.core.NetHelp;
import com.fairy.sso.core.SSOConstants;

public class CookieServlet extends HttpServlet {
    private static final long serialVersionUID = -1334093914490423930L;
    Logger log = Logger.getLogger(SSOServlet.class);
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	String ssoCookie=request.getParameter(SSOConstants.TICKET);
		System.out.println(this.getClass().getName()+",:::::::::::::::::::ssoCookie:"+ssoCookie);
		log.info(this.getClass().getName()+",:::::::::::::::::::ssoCookie:"+ssoCookie);
    	try{
        	if(null!=ssoCookie)
        	{
    			String domain=  NetHelp.getDomainOrIP(request);//NetHelp.getDomain(request);
        		System.out.println(this.getClass().getName()+"-------------------domain-------------------------"+domain);
        		log.info(this.getClass().getName()+"-------------------domain-------------------------"+domain);

        		
        		NetClientHelp nc=new NetClientHelp();
                String ticket =nc.getTicket(request);
                if(null==ticket||"".equals(ticket))
                {
            		boolean isIp=NetHelp.isIP(domain);
            		if(!isIp)
            		{
                		System.out.println("<-------------------   domain is not ip,"+domain+"------------------------->");
                		log.info("<-------------------   domain is not ip,"+domain+"------------------------->");
            			domain="."+domain;
            		}
            		else
            		{
            	        //本机ip
            			InetAddress addr = InetAddress.getLocalHost();
            	        String ssoAppCientIp=addr.getHostAddress().toString();//获得本机IP
	        			System.out.println("<=====================================netpay 配置为130时1.1.0版本获得的 本机ip:::::::"+ssoAppCientIp+"=====================================>");
	        			log.info("<=====================================netpay 配置为130时1.1.0版本获得的本机ip:::::::"+ssoAppCientIp+"=====================================>");
	        	        ssoAppCientIp=NetHelp.getLocalHostIp();
	        	 
	        			System.out.println("<=====================================netpay 配置为130时1.1.1版本获得的 本机ip:::::::"+ssoAppCientIp+"=====================================>");
	        			log.info("<=====================================netpay 配置为130时1.1.1版本获得的 本机ip:::::::"+ssoAppCientIp+"=====================================>");
	        	                    	        
            	        
            			domain=ssoAppCientIp;
                		System.out.println("<-------------------   domain is  ip,"+domain+"------------------------->");
                		log.info("<-------------------   domain is  ip,"+domain+"------------------------->");
            		}
                	
       			    //创建cookie
        			CookieHelper.addCookie(response, domain, "/", SSOConstants.SSO_COOKIE_KEY,ssoCookie, -1);
        			
            		System.out.println(this.getClass().getName()+"<++++++++++++++++++++++++++++++++++"+domain+"   设置cookie成功++++++++++++++++++++++++++++++++++++>");
            		log.info(this.getClass().getName()+"<++++++++++++++++++++++++++++++++++"+domain+"   设置cookie成功++++++++++++++++++++++++++++++++++++>");	
                }
                else
                {
              		System.out.println(this.getClass().getName()+"<++++++++++++++++++++++++++++++++++"+domain+"  cookie已经存在,无需再建++++++++++++++++++++++++++++++++++++>");
            		log.info(this.getClass().getName()+"<++++++++++++++++++++++++++++++++++"+domain+"  cookie已经存在,无需再建++++++++++++++++++++++++++++++++++++>");
                }
        		

    			//response.sendRedirect(redirectUrl);
    			//request.getRequestDispatcher(redirectUrl).forward(request, response);
        	}
    	}
    	catch(Exception e)
    	{
    		e.printStackTrace();
    	}
    	
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	
    	
    	
    	//set cookie
    	
       
    }

}


