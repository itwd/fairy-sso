package com.fairy.sso.client.local.login;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public interface ClientLocalLoginService {

	public boolean optLocalLogin(HttpServletRequest request,HttpServletResponse response,String userId);
}
