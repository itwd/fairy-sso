
/* 
功能：保存cookies函数  
参数：name，cookie名字；value，值 
*/  
function SetCookie(name,value){  
    var Days = 30*12;   //cookie 将被保存一年  
    var exp  = new Date();  //获得当前时间  
    exp.setTime(exp.getTime() + Days*24*60*60*1000);  //换成毫秒  
    document.cookie = name + "="+ escape (value) + ";expires=" + exp.toGMTString();  
}   
/* 
功能：获取cookies函数  
参数：name，cookie名字 
*/  
function getCookie(name){  
    var arr = document.cookie.match(new RegExp("(^| )"+name+"=([^;]*)(;|$)"));  
    if(arr != null){  
     return unescape(arr[2]);   
    }else{  
     return null;  
    }  
}   
/* 
功能：删除cookies函数  
参数：name，cookie名字 
*/  
  
function delCookie(name){  
    var exp = new Date();  //当前时间  
    exp.setTime(exp.getTime() - 1);  
    var cval=getCookie(name);  
    if(cval!=null) document.cookie= name + "="+cval+";expires="+exp.toGMTString();  
} 

/** 
 * 获取指定URL的参数值 
 * @param url  指定的URL地址 
 * @param name 参数名称 
 * @return 参数值 
 */  
function getUrlParam(url,name)
{  
    var pattern = new RegExp("[?&]"+name+"\=([^&]+)", "g");  
    var matcher = pattern.exec(url);  
    var items = null;  
    if(null != matcher)
    {  
            try{  
                   items = decodeURIComponent(decodeURIComponent(matcher[1]));  
            }catch(e)
            {  
                    try
                    {  
                            items = decodeURIComponent(matcher[1]);  
                    }catch(e)
                    {  
                            items = matcher[1];  
                    }  
            }  
    }  
    return items;  
}  

function setAppCookie(url)
{
	var cookie=getCookie("FAIRY-SSO-KEY");
	$.ajax({  
		  type:"get",  
		  dataType:"jsonp",
		  url: "http://"+url+"/setCookie.sso?ticket="+cookie+"&url="+url,
		  crossDomain:true, 
		  success: function(data)
		  {  
			  //alert(data);

		  },  
		  beforeSend:function()
		  {  
		  },  
		  complete:function(data,status)
		  {  
		  }  
		});
}
function delServerTicket()
{
	var cookie=getCookie("FAIRY-SSO-KEY");
	$.ajax({  
		  type:"get",  
		  dataType:"jsonp",
		  url: sso_server_url+"/logout2.do?ticket="+cookie,
		  crossDomain:true, 
		  success: function(data)
		  {  
			  //alert(data);

		  },  
		  beforeSend:function()
		  {  
		  },  
		  complete:function(data,status)
		  {  
		  }  
		});
}

function getDomainList()
{
	
	var url=window.location.href;
	if(cookie==null||cookie=="")
	{
		 var  ticket=getUrlParam(url,'ticket');
		 cookie=ticket;
	}
   
    
	
	$.ajax({  
		  type:"get",  
		  dataType:"jsonp",
		  url: sso_server_url+"/getDomainList.do",
		  crossDomain:true, 
		  success: function(data)
		  {  
			  //alert(data.code);
			  var obj=data.list;
			  for(var i=0;i<obj.length;i++)
			  {
				  var url=obj[i];
				  setAppCookie(url);
				  //alert(obj[i]);
			  }
		  },  
		  beforeSend:function()
		  {  
		  },  
		  complete:function(data,status)
		  {  
		  }  
		});
	
	
}
function ssoCrossDomain()
{
	try
	{
		var url=window.location.href;
	    //var params=window.location.search;
	    //var loc = url.substring(url.lastIndexOf('=')+1, url.length);
	    var  ssoCrossDomain=getUrlParam(url,'ssoCrossDomain');
	    //alert(isCrossDomainSSO);
	    if(ssoCrossDomain=="true"&&isCrossDomainSSO==true)
	    {
	    	 getDomainList();
	    }
	}
	catch(e)
	{
		
	}

    
}
function ssoLogout()
{
	try
	{
		$.ajax({  
			  type:"get",  
			  dataType:"jsonp",
			  url: sso_server_url+"/getDomainList.do",
			  crossDomain:true, 
			  success: function(data)
			  {  
				  //alert(data.code);
				  var obj=data.list;
				  for(var i=0;i<obj.length;i++)
				  {
					  var url=obj[i];
					  delCookie("FAIRY-SSO-KEY");
				  }
				  delServerTicket();
			  },  
			  beforeSend:function()
			  {  
			  },  
			  complete:function(data,status)
			  {  
			  }  
			});
	}
	catch(e)
	{
		
	}

    
}
$(document).ready(function(){  
     ssoCrossDomain();   
});  
