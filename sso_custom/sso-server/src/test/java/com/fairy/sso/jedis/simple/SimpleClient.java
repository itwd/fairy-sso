/**
 *  版权所有 Copyright@2016-04-05 12:06:56
 */

package com.fairy.sso.jedis.simple;

import org.junit.Test;


/**   
 * <p>
 * 测试独立redis 客户端
 * </p>
 * @Title: PerUserEntity
 * @Description: 个人用户
 * @author yyk1504@163.com
 * @date 2016-04-05 16:13:56
 * @version V1.0   
 *
 */
public class SimpleClient {
	
	@Test
	public void userCache(){
		/**
		//向缓存中保存对象
		UserDO zhuoxuan = new UserDO();
		zhuoxuan.setUserId(113445);
		zhuoxuan.setSex(1);
		zhuoxuan.setUname("卓轩");
		zhuoxuan.setUnick("zhuoxuan");
		zhuoxuan.setEmail("zhuoxuan@mogujie.com");
		//调用方法处理
		boolean reusltCache = RedisClient.set("zhuoxuan", zhuoxuan);
		if (reusltCache) {
			System.out.println("向缓存中保存对象成功。");
		}else{
			System.out.println("向缓存中保存对象失败。");
		}
		*/
	}
	
	
	@Test
	public void getUserInfo(){
		
		/**
		UserDO zhuoxuan = RedisClient.get("zhuoxuan",UserDO.class);
		if(zhuoxuan != null){
			System.out.println("从缓存中获取的对象，" + zhuoxuan.getUname() + "@" + zhuoxuan.getEmail());
		}
		*/
		
	}
	
	

}
