/**
 *  版权所有 Copyright@yyk1504@163.com
 */
package com.fairy.sso.cache.impl;

import java.io.IOException;

import com.fairy.sso.cache.SSOCache;
import com.fairy.sso.core.Ticket;
import com.fairy.sso.jedis.RedisStorage;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * [RedsCache.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-18 11:04:47]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-03-18 11:04:47]
 * @Version: [v1.0]
 */
public class RedisCache implements SSOCache{
	//ShardingRedisClient s=new ShardingRedisClient();
	RedisStorage redisService=new RedisStorage();
	@Override
	public Ticket get(String key) 
	{
		ObjectMapper mapper = new ObjectMapper();  
		String json=redisService.get(key);
		
		Ticket ticket=null;
        if(null!=json&&!"".equals(json))
	    {
			try {
				ticket = mapper.readValue(json, Ticket.class);
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
 
		return ticket;
	
	}

	@Override
	public boolean set(String key, Ticket ticket) {
		ObjectMapper mapper = new ObjectMapper();  
	    String json="";
	    boolean bl=false;
		try {
			if(null!=ticket)
			{
				json = mapper.writeValueAsString(ticket);
			}
			redisService.set(key, json);
			bl=true;
			return bl;
		} catch (JsonProcessingException e) {
			e.printStackTrace();
			return false;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
		
	}

	@Override
	public boolean delete(String key) {
		boolean bl=false;
		try {
			redisService.del(key);
			bl=true;
			return bl;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

}
