/**
 * 版权所有 Copyright@@2016-03-18 2:17:53
 */
package com.fairy.sso.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * [FairyControllerExceptionLog.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-18 2:17:53]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-03-18 2:17:53]
 * @Version: [v1.0]
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})    
@Retention(RetentionPolicy.RUNTIME)    
@Documented
public @interface FairyControllerExceptionLog {
	String description()  default "";  
}