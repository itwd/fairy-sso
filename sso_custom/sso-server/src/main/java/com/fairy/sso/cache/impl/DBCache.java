/**
 *  版权所有 Copyright@2016年4月6日下午2:35:27
 */
package com.fairy.sso.cache.impl;

import com.fairy.sso.cache.SSOCache;
import com.fairy.sso.core.Ticket;
import com.fairy.sso.entity.SSOUserEntity;
import com.fairy.sso.service.SSOService;

/**
 * [DBCache.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016年4月6日下午2:35:27]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016年4月6日下午2:35:27]
 * @Version: [v1.0]
 */

public class DBCache implements SSOCache{
	
	//WebApplicationContext wac = ContextLoader.getCurrentWebApplicationContext();
	@SuppressWarnings("unchecked")
	SSOService<SSOUserEntity> ssoService=(SSOService<SSOUserEntity>) wac.getBean("ssoService");
	
	//SSOService<SSOUserEntity> ssoService=(SSOService<SSOUserEntity>) WebContextFactoryUtil.getBeanById("ssoService");

	@Override
	public Ticket get(String key) {
		Ticket t=ssoService.loadTicket(key);
		return t;
	}

	@Override
	public boolean set(String key, Ticket ticket) {
		boolean bl=false;
		ssoService.saveTicket(ticket, key);
		bl=true;
		return bl;
	}

	@Override
	public boolean delete(String key) {
		boolean bl=false;
		ssoService.deleteTicketByKey(key);
		bl=true;
		return bl;
	}

}
