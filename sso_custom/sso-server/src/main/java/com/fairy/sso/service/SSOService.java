/**
 * 版权所有 Copyright@@2016-03-18 1:39:58
 */
package com.fairy.sso.service;

import java.util.Map;

import com.fairy.sso.core.Ticket;

/**
 * [UserService.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-18 1:39:58]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-03-18 1:39:58]
 * @Version: [v1.0]
 */
public interface SSOService<T> {

	/**通过用户名/手机号/邮箱和注册密码登录*/
	public T login(Map<String,Object>map,String accountNo,String passWord);

	
	public boolean saveTicket(Ticket ticket,String key);
	
	public Ticket loadTicket(String key);
	
	public boolean deleteTicketByKey(String key);
	

}
