/**
 *  版权所有 Copyright@2016年4月6日下午6:06:12
 */
package com.fairy.sso.consumer.impl;

import org.springframework.stereotype.Component;

import com.fairy.sso.consumer.SSOConsumer;
import com.fairy.sso.core.Ticket;

/**
 * [SSOConsumerImpl.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016年4月6日下午6:06:12]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016年4月6日下午6:06:12]
 * @Version: [v1.0]
 */
@Component("ssoConsumer")
public class SSOConsumerImpl implements SSOConsumer{

	//获得dubboservice
	
	@Override
	public Ticket get(String key) {
		// 调用dubbo服务
		return null;
	}

	@Override
	public boolean set(String key, Ticket ticket) {
		// 调用dubbo服务
		return false;
	}

	@Override
	public boolean delete(String key) {
		// 调用dubbo服务
		return false;
	}
	
}
