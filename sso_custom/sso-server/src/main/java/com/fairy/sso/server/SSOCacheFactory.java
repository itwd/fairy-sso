/**
 *   Copyright@yyk1504@163.com
 */
package com.fairy.sso.server;

import org.apache.log4j.Logger;

import com.fairy.sso.cache.SSOCache;
import com.fairy.sso.cache.impl.DBCache;
import com.fairy.sso.cache.impl.DubboCache;
import com.fairy.sso.cache.impl.HttpSessionCache;
import com.fairy.sso.cache.impl.JVMCache;
import com.fairy.sso.cache.impl.RedisCache;
import com.fairy.sso.core.SSOConstants;

/**
 * [SSOCacheFactory.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-18 11:26:03]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-03-18 11:26:03]
 * @Version: [v1.0]
 */
public class SSOCacheFactory extends Factory{
	private static Logger log = Logger.getLogger(SSOCacheFactory.class);
	@SuppressWarnings("unchecked")
	public SSOCache  createSSOCache(String cacheType) { 
		log.info("cacheType is:"+cacheType);
		SSOCache cache = null; 
	      try { 
	    	  if(cacheType.equals(SSOConstants.CACHE_TYPE_REDIS))
	    	  {
	    		  cache =new RedisCache();// (T) Class.forName(RedisCache.class.getName()).newInstance();
	    	  }
	    	  else
	    	  if(cacheType.equals(SSOConstants.CACHE_TYPE_SESSION))
	    	  {
	    		  //SessionCache s=new SessionCache();
	    		  
	    		  cache =new HttpSessionCache();// (T) Class.forName(SessionCache.class.getName()).newInstance();
	    	  }
	    	  else
	    	  if(cacheType.equals(SSOConstants.CACHE_TYPE_DB))
	    	  {
	    		  
	    		  cache =new DBCache();// (T) Class.forName(DBCache.class.getName()).newInstance();
	    	  }
	    	  else
	    	  if(cacheType.equals(SSOConstants.CACHE_TYPE_DUBBO))
	    	  {
	    		  
	    		  cache =new DubboCache();// (T) Class.forName(DBCache.class.getName()).newInstance();
	    	  }	    	  
	    	  else
	    	  {
	    		  cache = new JVMCache();//(T) Class.forName(JVMCache.class.getName()).newInstance();
	    	  }
	    	   
	      } catch (Exception e) { 
	        e.printStackTrace(); 
	      } 
	      log.info("cacheType is:"+cacheType+",cache is:"+cache.getClass().getName());
	      return cache; 
	    } 
}
