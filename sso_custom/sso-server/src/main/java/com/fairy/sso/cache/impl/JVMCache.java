/**
 *  版权所有 Copyright@yyk1504@163.com
 */
package com.fairy.sso.cache.impl;

import java.util.HashMap;
import java.util.Map;

import com.fairy.sso.cache.SSOCache;
import com.fairy.sso.core.Ticket;
/**
 * [JVMCache.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-18 11:04:47]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-03-18 11:04:47]
 * @Version: [v1.0]
 */
public class JVMCache implements SSOCache{

    private static Map<String, Ticket> TICKET_AND_NAME = new HashMap<String, Ticket>();

	@Override
	public Ticket get(String key) {
		
		autoCleanTicket();   
		
		return TICKET_AND_NAME.get(key);
	}

	private void autoCleanTicket() 
	{
		try
		{
			for(Map.Entry<String, Ticket> entry:TICKET_AND_NAME.entrySet())
			{    
				String key_1=entry.getKey();
				Ticket ticket=entry.getValue();
				
				long createTime=ticket.getCreateTime();
				
				long currentTime=System.currentTimeMillis(); 
				
				long time=currentTime-createTime;//时间相减比较。 
				if(time>((long)60000*120))
				{
					//120分钟{} 
					this.delete(key_1);
				}
			    System.out.println(entry.getKey()+"--->"+entry.getValue());    
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

	}

	@Override
	public boolean set(String key, Ticket ticket) {
		try
		{
			TICKET_AND_NAME.put(key, ticket);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
		
	}

	@Override
	public boolean delete(String key) {
		try
		{
			TICKET_AND_NAME.remove(key);
			return true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
			return false;
		}
		
	}
}