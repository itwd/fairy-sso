/**
 *  版权所有 Copyright@2016-03-31 16:13:56
 */
package com.fairy.sso.biz;

import java.util.List;
import java.util.Map;

import com.fairy.common.page.Page;
import com.fairy.sso.entity.UserEntity;

/**
 * [UserBiz.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-31 16:13:56]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-03-31 16:13:56]
 * @Version: [v1.0]
 */


public interface UserBiz {
	public void add(UserEntity entity);
	public UserEntity getById(java.lang.String userId);
	public List<UserEntity> getAlls();
	
	@SuppressWarnings("rawtypes")
	public List<UserEntity> getPageList(UserEntity entity,Map assistMap,Page page);
	
	
	public void edit(UserEntity entity);
	public void deleteById(java.lang.String userId);
	
	 public UserEntity login(UserEntity entity);
}


