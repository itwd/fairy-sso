/**
 * 版权所有 Copyright@@2016-03-18 10:39:05
 */
package com.fairy.sso.cache;

import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import com.fairy.sso.core.Ticket;

/**
 * [SSOCache.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-18 10:39:05]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-03-18 10:39:05]
 * @Version: [v1.0]
 */
public interface SSOCache {
    
	WebApplicationContext wac = ContextLoader.getCurrentWebApplicationContext();
	//@SuppressWarnings("unchecked")
	//SSOService<SSOUserEntity> ssoService=(SSOService<SSOUserEntity>) wac.getBean("ssoService");
	
	Ticket get(String key);

	boolean set( String key,Ticket ticket);

	boolean delete(String key);
	

	
}
