/**
 *  版权所有 Copyright@yyk1504@163.com
 */
package com.fairy.sso.config;

import java.util.List;

import org.springframework.stereotype.Component;

/**
 * [ApiCollection.java]
 *
 * @ProjectName: [dubbo_fairy_demo_web]
 * @Author: [waysun]
 * @CreateDate: [2016-03-18 下午3:41:15]
 * @Update: [说明本次修改内容] BY [waysun] [2016-03-18下午3:41:15]
 * @Version: [v1.0]
 */
@Component()
public class ApiCollection {

	private List<String> apiList;

	public List<String> getApiList() {
		return apiList;
	}

	public void setApiList(List<String> apiList) {
		this.apiList = apiList;
	}
	
}



