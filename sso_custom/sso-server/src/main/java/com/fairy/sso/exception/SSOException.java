/**
 *  fairy 版权所有 Copyright@2016年2月18日
 */
package com.fairy.sso.exception;

/**
 * [BusinessException.java]
 *
 * @ProjectName: [dubbo_fairy_common]
 * @Author: [waysun]
 * @CreateDate: [2016年2月18日 上午10:50:49]
 * @Update: [说明本次修改内容] BY [waysun] [2016年2月18日 上午10:50:49]
 * @Version: [v1.0]
 */

public class SSOException extends RuntimeException {

    private static final long serialVersionUID = -418389432305176131L;

    public SSOException() {
        // TODO Auto-generated constructor stub
    }

    public SSOException(String message) {//{errorCode:00001,message:"kdjaeie"}
        super(message);
        // TODO Auto-generated constructor stub
    }

    public SSOException(String message, Throwable e) {
        super(message, e);
        // TODO Auto-generated constructor stub
    }

    public SSOException(Throwable t) {
        super(t);
        // TODO Auto-generated constructor stub
    }

}



