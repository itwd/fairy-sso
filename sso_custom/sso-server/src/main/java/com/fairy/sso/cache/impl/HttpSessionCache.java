/**
 *  版权所有 Copyright@2016年4月1日下午4:11:49
 */
package com.fairy.sso.cache.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import com.fairy.sso.cache.SSOCache;
import com.fairy.sso.core.Ticket;

/**
 * [SessionCache.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016年4月1日下午4:11:49]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016年4月1日下午4:11:49]
 * @Version: [v1.0]
 */
public class HttpSessionCache implements SSOCache{

	//@Autowired  
	//private HttpServletRequest request; //这里可以获取到request
	HttpServletRequest request = ((ServletRequestAttributes)RequestContextHolder.getRequestAttributes()).getRequest();  
	private HttpSession session=request.getSession();


	
	@Override
	public Ticket get(String key) {
		Ticket t=(Ticket)session.getAttribute(key);
		return t;
	}

	@Override
	public boolean set(String key, Ticket ticket) {
		boolean bl=false;
		session.setAttribute(key, ticket);
		bl=true;
		return bl;
	}

	@Override
	public boolean delete(String key) {
		boolean bl=false;
		session.removeAttribute(key);
		bl=true;
		return bl;
	}


}
