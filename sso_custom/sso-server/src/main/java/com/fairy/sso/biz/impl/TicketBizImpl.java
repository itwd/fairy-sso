/**
 *  fairy 版权所有 Copyright@2016-03-31 16:44:46
 */
package com.fairy.sso.biz.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fairy.common.page.Page;
import com.fairy.common.page.SearchCountEntity;
import com.fairy.common.page.SearchPageEntity;
import com.fairy.sso.annotation.FairyBizExceptionLog;
import com.fairy.sso.biz.TicketBiz;
import com.fairy.sso.dao.TicketMapper;
import com.fairy.sso.entity.TicketEntity;

/**
 * [TicketBizImpl.java]
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-31 16:44:46]
 * @Update: [说明本次修改内容] BY [yyk1504@163.com] [2016-03-31 16:44:46]
 * @Version: [v1.0]
 */
@Component("ticketBiz")
public class TicketBizImpl implements TicketBiz {
	Logger logger = Logger.getLogger(TicketBizImpl.class);
	@Autowired(required=false)
	private TicketMapper ticketMapper;
	
	@FairyBizExceptionLog(description = "保存票据信息")
	@Transactional(propagation=Propagation.REQUIRED) 
	public void save(TicketEntity entity) { 
		ticketMapper.insert(entity);
	}

	@FairyBizExceptionLog(description = "通过id获得票据信息")
	@Transactional(propagation=Propagation.REQUIRED,readOnly=true)
	public TicketEntity getById(java.lang.String key) {
		// TODO Auto-generated method stub
		return ticketMapper.selectByPrimaryKey(key);
	}
	
	@FairyBizExceptionLog(description = "获得所有票据信息")
	@Transactional(propagation=Propagation.REQUIRED,readOnly=true)
	public List<TicketEntity> getAlls() {
		List<TicketEntity>list=ticketMapper.selectAlls();
		return list;
	}
	@SuppressWarnings({ "rawtypes"})
	@FairyBizExceptionLog(description = "分页获得票据信息")
	@Transactional(propagation=Propagation.REQUIRED,readOnly=true)
	public List<TicketEntity> getPageList(TicketEntity entity,Map assistMap,Page page) 
	{
		SearchCountEntity<TicketEntity> searchCountEntity = new SearchCountEntity<TicketEntity>();  
		searchCountEntity.setEntity(entity);
		searchCountEntity.setAssistMap(assistMap);
		int count= ticketMapper.getCount(searchCountEntity);
		page.setRowTotal(count);
		SearchPageEntity<TicketEntity> searchPageEntity = new SearchPageEntity<TicketEntity>();  
        searchPageEntity.setPage(page);  
        searchPageEntity.setEntity(entity);
        searchPageEntity.setAssistMap(assistMap);
        List<TicketEntity> list=ticketMapper.getPageList(searchPageEntity);
        return list;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED,isolation=Isolation.DEFAULT)
	public void edit(TicketEntity entity) {
		// TODO Auto-generated method stub
		ticketMapper.edit(entity);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED,isolation=Isolation.DEFAULT)
	public void deleteById(java.lang.String key) {
		// TODO Auto-generated method stub
		ticketMapper.deleteById(key);
	}


	
}



