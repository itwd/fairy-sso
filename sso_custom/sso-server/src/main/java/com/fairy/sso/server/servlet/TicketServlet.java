package com.fairy.sso.server.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.fairy.sso.cache.SSOCache;
import com.fairy.sso.config.FairyConfig;
import com.fairy.sso.core.Ticket;
import com.fairy.sso.server.Factory;
import com.fairy.sso.server.SSOCacheFactory;

public class TicketServlet extends HttpServlet {
    private static final long serialVersionUID = 5964206637772848290L;
	@Autowired(required=true)
	private FairyConfig fairyConfig;
	private static Logger log = Logger.getLogger(TicketServlet.class);
	
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        super.doGet(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String ticket = request.getParameter("ticket");
        System.out.println("保持心跳:::::::::::::::::::ticket:::::::::::::::"+ticket);
       // String username = JVMCache.TICKET_AND_NAME.get(ticket);
        //JVMCache.TICKET_AND_NAME.remove(ticket);
  
        Factory factory = new SSOCacheFactory(); 
        String cacheType=fairyConfig.getCacheType();
        log.info("session 存储类型为:"+cacheType);
        SSOCache cache = factory.createSSOCache(cacheType);
        Ticket t=cache.get(ticket);
        log.info("Ticket:"+t.toString());
        String username=t.getUserId();
        //cache.delete(ticket);
        
        
        
        PrintWriter writer = response.getWriter();
        writer.write(username);
    }
    public void init(ServletConfig config) throws ServletException {  
        super.init(config);  
    SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this, config.getServletContext());  
}  
}