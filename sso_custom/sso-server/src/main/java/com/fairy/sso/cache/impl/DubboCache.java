/**
 *  版权所有 Copyright@2016年4月6日下午5:37:29
 */
package com.fairy.sso.cache.impl;

import com.fairy.sso.cache.SSOCache;
import com.fairy.sso.consumer.SSOConsumer;
import com.fairy.sso.core.Ticket;

/**
 * [DubboCache.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016年4月6日下午5:37:29]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016年4月6日下午5:37:29]
 * @Version: [v1.0]
 */
public class DubboCache implements SSOCache{

	SSOConsumer ssoConsumer=(SSOConsumer) wac.getBean("ssoConsumer");
	@Override
	public Ticket get(String key) {
		return ssoConsumer.get(key);
	}

	@Override
	public boolean set(String key, Ticket ticket) {
		return ssoConsumer.set(key, ticket);
	}

	@Override
	public boolean delete(String key) {
		return ssoConsumer.delete(key);
	}

}
