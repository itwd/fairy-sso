/**
 * 版权所有 Copyright@@2016-03-18 1:41:25
 */
package com.fairy.sso.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 * [SSOConfig.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-18 01:41:25]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-03-18 01:41:25]
 * @Version: [v1.0]
 */

@Component("fairyConfig")
public class FairyConfig {
	@Value("${cache_type}")  
    private String cacheType;
	
	//@Value("${sso.client.app_url}")
	//private String ssoClientUrl;
	
	
	@Value("${sso.cross_domain_client.app_url}")
	private String ssoCrossDomainClientAppUrl;
	
	@Value("${cross_domain}")
	private boolean crossDomain;
	

	@Value("${redis.ip}")
	private String redisIp;
	
	@Value("${redis.port}")
	private String redisPort;

	
	@Value("${sso.cookie.domain}")
	private String domain;
	
	@Value("${sso.cookie.path}")
	private String path;
	
	@Value("${sso.cookie.maxage}")
	private int maxAge;
	
	@Value("${sso.cookie.secure}")
	private boolean secure;
	
	@Value("${sso.cookie.httponly}")
	private boolean httpOnly;
	
	@Value("${sso.cookie.name}")
	private String cookieName;

	

	@Value("${redis.pool.maxActive}")  
    private String maxActive;
	
	@Value("${redis.pass}")  
    private String redisPass;
	
	@Value("${redis.pool.maxWait}")  
    private String maxWait;
	
	
	@Value("${redis.pool.minIdle}")  
    private String minIdle;
	
	@Value("${redis.pool.maxIdle}")  
    private String maxIdle;
	
	@Value("${redis.pool.maxWaitMillis}")  
    private String maxWaitMillis;
	

	
	@Value("${redis.pool.testOnBorrow}")  
    private boolean testOnBorrow;
	
	
	@Value("${redis.pool.testOnReturn}")  
    private boolean testOnReturn;
	
	
	@Value("${redis.pool.maxTotal}")  
    private String maxTotal;
	
	
	@Value("${redis.database}")  
    private String redisDatabase;
	
	@Value("${index_page_name}")  
    private String indexPageName;	
	/**
	@Value("${redis.address}")  
    private String address;
	*/

	/*
	@Value("${sso.ticket.secretkey}")  
	private String ssoTicketSecretkey;
	*/

	public String getCacheType() {
		return cacheType;
	}
	public String getRedisIp() {
		return redisIp;
	}
	
	public String getRedisPort() {
		return redisPort;
	}

	public String getDomain() {
		return domain;
	}
	public String getPath() {
		return path;
	}
	public int getMaxAge() {
		return maxAge;
	}
	public boolean isSecure() {
		return secure;
	}
	public boolean isHttpOnly() {
		return httpOnly;
	}
	public String getCookieName() {
		return cookieName;
	}
	public String getMaxActive() {
		return maxActive;
	}
	public String getMaxIdle() {
		return maxIdle;
	}
	public String getMaxWait() {
		return maxWait;
	}
	
	/**
	public String getAddress() {
		return address;
	}
	*/
	public String getRedisPass() {
		return redisPass;
	}
	public String getMinIdle() {
		return minIdle;
	}
	public String getMaxWaitMillis() {
		return maxWaitMillis;
	}
	public boolean isTestOnBorrow() {
		return testOnBorrow;
	}
	public boolean isTestOnReturn() {
		return testOnReturn;
	}
	public String getMaxTotal() {
		return maxTotal;
	}
	public String getRedisDatabase() {
		return redisDatabase;
	}

	/**
	public String getSsoClientUrl() {
		return ssoClientUrl;
	}
	*/
	public String getSsoCrossDomainClientAppUrl() {
		return ssoCrossDomainClientAppUrl;
	}
	public boolean getCrossDomain() {
		return crossDomain;
	}
	public String getIndexPageName() {
		return indexPageName;
	}	
}
