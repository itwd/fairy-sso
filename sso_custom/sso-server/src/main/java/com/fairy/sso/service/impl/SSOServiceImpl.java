/**
 * 版权所有 Copyright@@2016-03-18 1:40:18
 */
package com.fairy.sso.service.impl;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fairy.sso.biz.TicketBiz;
import com.fairy.sso.biz.UserBiz;
import com.fairy.sso.core.SSOResult;
import com.fairy.sso.core.Ticket;
import com.fairy.sso.entity.SSOUserEntity;
import com.fairy.sso.entity.TicketEntity;
import com.fairy.sso.entity.UserEntity;
import com.fairy.sso.service.SSOService;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * [UserServiceImpl.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-18 1:40:18]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-03-18 1:40:18]
 * @Version: [v1.0]
 */
@Service("ssoService")
public class SSOServiceImpl implements SSOService<SSOResult>{

	
	
	Logger log = Logger.getLogger(SSOServiceImpl.class);
	
	//@Autowired(required=false)
	private UserBiz userBiz;
	
	//@Autowired
	//@Autowired(required=false)
	private TicketBiz ticketBiz;
	
	
	//@Autowired(required=true)
	//private FlowService flowService;
	
	@Override
	public SSOResult login(Map<String,Object>param,String accountNo, String passWord) {
		 long start = System.currentTimeMillis();  
		SSOResult redult=new SSOResult();
		SSOUserEntity ssoUserEntity=null;
		String code="111111";
		String msg="登录失败";
		/**
		Map<String, Object> map = null;
		try {
			map = flowService.execute(param);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if(null!=map&&map.size()>0)
		{
		    String codeReturn="0";//map.get("errorCode").toString();
			if(null!=codeReturn&&!"".equals(codeReturn))
			{
				if(codeReturn.equals("0"))
				{
					ssoUserEntity=new SSOUserEntity();
		            Map<String,Object>dataMaps=(Map<String, Object>) map.get("dataMap");
					String loginId=(String)dataMaps.get("loginId");
					ssoUserEntity.setUserId(loginId);	 
					code="000000";
					msg="登录成功";
				}
				else
				{
					//code=map.get("errorCode").toString();
					msg=map.get("errorMsg").toString();
				}

			}
		}
        */
		

		String em = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
		//String ph = " ^[1][358][0-9]{9}$";
		String ph = "^[1][3578]\\d{9}$";
		UserEntity entity=new UserEntity();
		if(accountNo.matches(em))
		{    //邮箱登录
			entity.setEmail(accountNo);
			log.info("邮箱登录:"+accountNo);
		} 
		else if(accountNo.matches(ph))
		{
			//手机号登录
			entity.setMobile(accountNo);
			log.info("手机号登录:"+accountNo);
		}
		else
		{
			//就是用户名登录
			//user.setUsername(username);
			entity.setUserName(accountNo);
			log.info("用户名登录:"+accountNo);
		}
		entity.setUserPass(passWord);
		entity=userBiz.login(entity);
		if(null!=entity)
		{
			ssoUserEntity=new SSOUserEntity();
			ssoUserEntity.setAccount(accountNo);
			ssoUserEntity.setUserId(entity.getUserId());
			ssoUserEntity.setUserName(entity.getUserName());
			code="000000";
			msg="successful";
		}
		
		redult.setCode(code);
		redult.setMsg(msg);
		redult.setSsoUserEntity(ssoUserEntity);
		long end = System.currentTimeMillis();        
		log.info("<<===================执行耗时:"+(end-start)+"毫秒============================>>"); 
		return redult;
	}


	@Override
	public boolean saveTicket(Ticket ticket,String key) {
		boolean bl=false;
		TicketEntity ticketEntity=new TicketEntity();
		ticketEntity.setEnable(0);
		ticketEntity.setKey(key);
		ticketEntity.setCreateTime(ticket.getCreateTime()+"");
		ticketEntity.setTicket(ticket.toString());
		
		ticketBiz.save(ticketEntity);
		bl=true;
		return bl;
	}

	@Override
	public Ticket loadTicket(String key) {
		TicketEntity ticketEntity=ticketBiz.getById(key);
		Ticket ticket=new Ticket();
		if(null!=ticketEntity)
		{
			String st=ticketEntity.getTicket();
			ObjectMapper mapper = new ObjectMapper(); 
			try {
				ticket = mapper.readValue(st, Ticket.class);
				log.info("Ticket is::"+ticket.toString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return ticket;
	}
	@Override
	public boolean deleteTicketByKey(String key) {
		boolean bl=false;

		
		ticketBiz.deleteById(key);;
		bl=true;
		return bl;
	}

	/***************ynet特别版*************/


}
