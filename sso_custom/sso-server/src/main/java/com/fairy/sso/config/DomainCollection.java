/**
 *  版权所有 Copyright@2016年4月6日上午11:16:25
 */
package com.fairy.sso.config;

import java.util.List;

import org.springframework.stereotype.Component;

/**
 * [DomainList.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016年4月6日上午11:16:25]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016年4月6日上午11:16:25]
 * @Version: [v1.0]
 */
@Component()
public class DomainCollection {

	private List<String> domainList;

	public List<String> getDomainList() {
		return domainList;
	}

	public void setDomainList(List<String> domainList) {
		this.domainList = domainList;
	}
	
}
