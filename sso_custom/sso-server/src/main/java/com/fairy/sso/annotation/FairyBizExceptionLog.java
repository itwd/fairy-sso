/**
 *  fairy 版权所有 Copyright@2016年2月22日
 */
package com.fairy.sso.annotation;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * [FairyBizExceptionLog.java]
 *
 * @ProjectName: [dubbo_fairy_common]
 * @Author: [waysun]
 * @CreateDate: [2016年2月22日 下午4:46:32]
 * @Update: [说明本次修改内容] BY [waysun] [2016年2月22日 下午4:46:32]
 * @Version: [v1.0]
 */
@Target({ElementType.PARAMETER, ElementType.METHOD})    
@Retention(RetentionPolicy.RUNTIME)    
@Documented
public @interface FairyBizExceptionLog {
	String description()  default "";  
}



