/**
 *  版权所有 Copyright@2016-03-31 16:13:56
 */
package com.fairy.sso.dao;

import java.util.List;

import com.fairy.common.page.SearchCountEntity;
import com.fairy.common.page.SearchPageEntity;
import com.fairy.sso.entity.UserEntity;

/**
 * [UserMapper.java]
 *
 * @ProjectName: [sso-server]
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-31 16:13:56]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-03-31 16:13:56]
 * @Version: [v1.0]
 */

public interface UserMapper {

    public int insert(UserEntity entity);

    public UserEntity selectByPrimaryKey(java.lang.String userId);

    public List<UserEntity> selectAlls();
      
    public List<UserEntity> getPageList(SearchPageEntity<UserEntity> searchPageEntity);  
  
    public int getCount(SearchCountEntity<UserEntity> searchCountEntity); 
    
    public void edit(UserEntity entity);
    
    public void deleteById(java.lang.String userId);
    
    public UserEntity login(UserEntity entity);
}
