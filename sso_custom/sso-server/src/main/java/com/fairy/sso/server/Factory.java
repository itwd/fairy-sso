/**
 *  版权所有 Copyright@yyk1504@163.com
 */
package com.fairy.sso.server;

import com.fairy.sso.cache.SSOCache;

/**
 * [Factory.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-18 11:24:51]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-03-18 11:24:51]
 * @Version: [v1.0]
 */
public abstract class Factory{

    public abstract <T extends SSOCache> T createSSOCache(String cacheType); 
}
