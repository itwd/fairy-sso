/**
 *  版权所有 Copyright@2016年4月14日上午11:33:11
 */
package com.fairy.sso.jedis;

import java.util.Set;

import org.springframework.data.redis.connection.jedis.JedisConnection;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.web.context.ContextLoader;
import org.springframework.web.context.WebApplicationContext;

import redis.clients.jedis.Jedis;

/**
 * [JedisTool.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016年4月14日上午11:33:11]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016年4月14日上午11:33:11]
 * @Version: [v1.0]
 */

public class RedisStorage {

    //操作redis客户端
    private static Jedis jedis;
    WebApplicationContext wac = ContextLoader.getCurrentWebApplicationContext();
	JedisConnectionFactory jedisConnectionFactory=(JedisConnectionFactory) wac.getBean("jedisConnectionFactory");

	/**
     * 通过key删除（字节）
     * @param key
     */
    public void del(byte [] key){
        this.getJedis().del(key);
    }
    /**
     * 通过key删除
     * @param key
     */
    public void del(String key){
        this.getJedis().del(key);
    }

    /**
     * 添加key value 并且设置存活时间(byte)
     * @param key
     * @param value
     * @param liveTime
     */
    public void set(byte [] key,byte [] value,int liveTime){
        this.set(key, value);
        this.getJedis().expire(key, liveTime);
    }
    /**
     * 添加key value 并且设置存活时间
     * @param key
     * @param value
     * @param liveTime
     */
    public void set(String key,String value,int liveTime){
        this.set(key, value);
        this.getJedis().expire(key, liveTime);
    }
    /**
     * 添加key value
     * @param key
     * @param value
     */
    public void set(String key,String value){
        this.getJedis().set(key, value);
    }
    /**添加key value (字节)(序列化)
     * @param key
     * @param value
     */
    public void set(byte [] key,byte [] value){
        this.getJedis().set(key, value);
    }
    /**
     * 获取redis value (String)
     * @param key
     * @return
     */
    public String get(String key){
        String value = this.getJedis().get(key);
        return value;
    }
    /**
     * 获取redis value (byte [] )(反序列化)
     * @param key
     * @return
     */
    public byte[] get(byte [] key){
        return this.getJedis().get(key);
    }

    /**
     * 通过正则匹配keys
     * @param pattern
     * @return
     */
    public Set<String> keys(String pattern){
        return this.getJedis().keys(pattern);
    }

    /**
     * 检查key是否已经存在
     * @param key
     * @return
     */
    public boolean exists(String key){
        return this.getJedis().exists(key);
    }
    /**
     * 清空redis 所有数据
     * @return
     */
    public String flushDB(){
        return this.getJedis().flushDB();
    }
    /**
     * 查看redis里有多少数据
     */
    public long dbSize(){
        return this.getJedis().dbSize();
    }
    /**
     * 检查是否连接成功
     * @return
     */
    public String ping(){
        return this.getJedis().ping();
    }
    /**
     * 获取一个jedis 客户端
     * @return
     */
    private Jedis getJedis(){
    	/**
        if(jedis == null){
            return jedisConnectionFactory.getShardInfo().createResource();
        }
        return jedis;
        */
    	 if (jedis == null) {  
             JedisConnection  jedisConnection = jedisConnectionFactory.getConnection();  
             jedis = jedisConnection.getNativeConnection();  
          return jedis;  
      }  
      return jedis;  
    }
    public RedisStorage (){

    }

}
