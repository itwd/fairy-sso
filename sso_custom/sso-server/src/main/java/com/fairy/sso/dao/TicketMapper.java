/**
 *  版权所有 Copyright@2016-03-31 16:44:46
 */
package com.fairy.sso.dao;

import java.util.List;

import com.fairy.common.page.SearchCountEntity;
import com.fairy.common.page.SearchPageEntity;
import com.fairy.sso.entity.TicketEntity;

/**
 * [TicketMapper.java]
 *
 * @ProjectName: [TicketMapper]
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-31 16:44:46]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-03-31 16:44:46]
 * @Version: [v1.0]
 */

public interface TicketMapper {

    public int insert(TicketEntity entity);

    public TicketEntity selectByPrimaryKey(java.lang.String key);

    public List<TicketEntity> selectAlls();
      
    public List<TicketEntity> getPageList(SearchPageEntity<TicketEntity> searchPageEntity);  
  
    public int getCount(SearchCountEntity<TicketEntity> searchCountEntity); 
    
    public void edit(TicketEntity entity);
    
    public void deleteById(java.lang.String key);
}
