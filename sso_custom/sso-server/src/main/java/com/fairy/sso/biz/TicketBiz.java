/**
 *  版权所有 Copyright@2016-03-31 16:44:46
 */
package com.fairy.sso.biz;

import java.util.List;
import java.util.Map;

import com.fairy.common.page.Page;
import com.fairy.sso.entity.TicketEntity;

/**
 * [TicketBiz.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-31 16:44:46]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-03-31 16:44:46]
 * @Version: [v1.0]
 */


public interface TicketBiz {
	public void save(TicketEntity entity);
	public TicketEntity getById(java.lang.String key);
	public List<TicketEntity> getAlls();
	
	@SuppressWarnings("rawtypes")
	public List<TicketEntity> getPageList(TicketEntity entity,Map assistMap,Page page);
	
	
	public void edit(TicketEntity entity);
	public void deleteById(java.lang.String key);
}


