/**
 *  版权所有 Copyright@2016年4月6日下午6:05:58
 */
package com.fairy.sso.consumer;

import com.fairy.sso.core.Ticket;

/**
 * [SSOConsumer.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016年4月6日下午6:05:58]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016年4月6日下午6:05:58]
 * @Version: [v1.0]
 */
public interface SSOConsumer {
	Ticket get(String key);
	boolean set( String key,Ticket ticket);
	boolean delete(String key);
}
