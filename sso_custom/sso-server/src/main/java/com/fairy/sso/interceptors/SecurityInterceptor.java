package com.fairy.sso.interceptors;

import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import com.fairy.sso.config.FairyConfig;

public class SecurityInterceptor  implements HandlerInterceptor{
	@SuppressWarnings("unused")
	@Autowired(required=true)
	private FairyConfig config;
	@Override
	public boolean preHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler) throws Exception {
		//String sqlReg = config.getSqlScript();//"(?:')|(?:--)|(/\\*(?:.|[\\n\\r])*?\\*/)|"  
	            //+ "(\\b(select|update|and|or|delete|insert|trancate|char|into|substr|ascii|declare|exec|count|master|into|drop|execute)\\b)";  
		//String jsReg = fairyConfig.getJsScript();//"<script.*>.*<\\/script\\s*>"; 
		//boolean bl=isValid(sqlReg,""); 
		return false;
	}

	@Override
	public void postHandle(HttpServletRequest request,
			HttpServletResponse response, Object handler,
			ModelAndView modelAndView) throws Exception {

		
	}

	@Override
	public void afterCompletion(HttpServletRequest request,
			HttpServletResponse response, Object handler, Exception ex)
			throws Exception {
		// TODO Auto-generated method stub
		
	}
	/*************************************************************************** 
	 * 参数校验 
	 *  
	 * @param str 
	 */  
	@SuppressWarnings("unused")
	private static boolean isValid(String reg,String str) {  
		 Pattern pattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE); 
	    if (pattern.matcher(str).find()) {  
	        return false;  
	    } 
	    else
	        return true;  
	} 
}
