/**
 *  fairy 版权所有 Copyright@2016-03-31 16:13:56
 */
package com.fairy.sso.biz.impl;

import java.util.List;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import com.fairy.common.page.Page;
import com.fairy.common.page.SearchCountEntity;
import com.fairy.common.page.SearchPageEntity;
import com.fairy.sso.annotation.FairyBizExceptionLog;
import com.fairy.sso.biz.UserBiz;
import com.fairy.sso.dao.UserMapper;
import com.fairy.sso.entity.UserEntity;

/**
 * [UserBizImpl.java]
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-31 16:13:56]
 * @Update: [说明本次修改内容] BY [yyk1504@163.com] [2016-03-31 16:13:56]
 * @Version: [v1.0]
 */
@Component("userBiz")
public class UserBizImpl implements UserBiz {
	Logger logger = Logger.getLogger(UserBizImpl.class);
	//@Autowired(required=false)
	private UserMapper userMapper;
	
	@FairyBizExceptionLog(description = "保存个人用户信息")
	@Transactional(propagation=Propagation.REQUIRED) 
	public void add(UserEntity entity) { 
		userMapper.insert(entity);
	}

	@FairyBizExceptionLog(description = "通过id获得个人用户信息")
	@Transactional(propagation=Propagation.REQUIRED,readOnly=true)
	public UserEntity getById(java.lang.String userId) {
		// TODO Auto-generated method stub
		return userMapper.selectByPrimaryKey(userId);
	}
	
	@FairyBizExceptionLog(description = "获得所有个人用户信息")
	@Transactional(propagation=Propagation.REQUIRED,readOnly=true)
	public List<UserEntity> getAlls() {
		List<UserEntity>list=userMapper.selectAlls();
		return list;
	}
	@SuppressWarnings({ "rawtypes"})
	@FairyBizExceptionLog(description = "分页获得个人用户信息")
	@Transactional(propagation=Propagation.REQUIRED,readOnly=true)
	public List<UserEntity> getPageList(UserEntity entity,Map assistMap,Page page) 
	{
		SearchCountEntity<UserEntity> searchCountEntity = new SearchCountEntity<UserEntity>();  
		searchCountEntity.setEntity(entity);
		searchCountEntity.setAssistMap(assistMap);
		int count= userMapper.getCount(searchCountEntity);
		page.setRowTotal(count);
		SearchPageEntity<UserEntity> searchPageEntity = new SearchPageEntity<UserEntity>();  
        searchPageEntity.setPage(page);  
        searchPageEntity.setEntity(entity);
        searchPageEntity.setAssistMap(assistMap);
        List<UserEntity> list=userMapper.getPageList(searchPageEntity);
        return list;
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED,isolation=Isolation.DEFAULT)
	public void edit(UserEntity entity) {
		// TODO Auto-generated method stub
		userMapper.edit(entity);
	}

	@Override
	@Transactional(propagation=Propagation.REQUIRED,isolation=Isolation.DEFAULT)
	public void deleteById(java.lang.String userId) {
		// TODO Auto-generated method stub
		userMapper.deleteById(userId);
	}

	@Override
	public UserEntity login(UserEntity entity) {
		// TODO Auto-generated method stub
		return userMapper.login(entity);
	}


	
}



