/**
 * 版权所有 Copyright@@2016-03-18 1:40:18
 */
package com.fairy.sso.service.impl;

import java.io.IOException;
import java.util.Map;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fairy.sso.biz.TicketBiz;
import com.fairy.sso.biz.UserBiz;
import com.fairy.sso.core.SSOResult;
import com.fairy.sso.core.Ticket;
import com.fairy.sso.entity.SSOUserEntity;
import com.fairy.sso.entity.TicketEntity;
import com.fairy.sso.entity.UserEntity;
import com.fairy.sso.service.SSOService;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.ifp.core.flow.service.FlowService;
//import com.ifp.core.exception.BaseException;
//import com.ifp.core.flow.service.FlowService;

/**
 * [UserServiceImpl.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016-03-18 1:40:18]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016-03-18 1:40:18]
 * @Version: [v1.0]
 */
@Service("ssoService")
public class SSOServiceImpl implements SSOService<SSOResult>{

	
	
	Logger log = Logger.getLogger(SSOServiceImpl.class);
	@Autowired
	private UserBiz userBiz;
	
	@Autowired
	private TicketBiz ticketBiz;
	
	@Autowired(required=true)
	private FlowService flowService;
	
	@SuppressWarnings("null")
	@Override
	public SSOResult login(Map<String,Object>param,String accountNo, String passWord) {
	    long start = System.currentTimeMillis();  
		SSOResult redult=new SSOResult();
		SSOUserEntity ssoUserEntity=null;
		String code="111111";
		String msg="登录失败";
		/**
		try {
		
				//加密公钥
				String publicExponent="77028117008151838397561091386153544713100881543763346306434348477078919505223572600452586695466776763237428215406263079117485404303606642445313272225818920348531532469258998853829694114255564458579379644055843016364532433735021025438101101510342034515860929484336454371385666187771495033051767213936234583425";
				//加密摸
				String modulus="100951719880879235153000864819708532173389042789537624022813020360403174571328186167977066237267650856543041494082315390475595747347227703194386592046224364935159185611020219601794564587615099602643355523045466052459398188407659087384452961625136991907488976759310385499683865174575673366664452848101413304051";

				//调用公钥需要加密方法
				RSAPublicKey pubKey = RSAUtils.getPublicKey(modulus, publicExponent); 
				//加密后密文

				String	result = RSAUtils.encryptByPublicKey(passWord, pubKey);
	
				Map<String, Object> map=null;
				map = flowService.execute(param);
				if(null!=map&&map.size()>0)
				{
			        String codeReturn="0";//map.get("errorCode").toString();
					if(null!=codeReturn&&!"".equals(codeReturn))
					{
						if(codeReturn.equals("0"))
						{
							ssoUserEntity=new SSOUserEntity();
		                    Map<String,Object>dataMaps=(Map<String, Object>) map.get("dataMap");
							String loginId=(String)dataMaps.get("loginId");
							ssoUserEntity.setUserId(loginId);	 
							code="000000";
							msg="登录成功";
						}
						else
						{
							//code=map.get("errorCode").toString();
							msg=map.get("errorMsg").toString();
						}

					}
				}
		
	
			

	
		} catch (Exception e) {
			e.printStackTrace();
		}
        
		*/

		String em = "^\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";
		//String ph = " ^[1][358][0-9]{9}$";
		String ph = "^[1][3578]\\d{9}$";
		UserEntity entity=new UserEntity();
		if(accountNo.matches(em))
		{    //邮箱登录
			entity.setEmail(accountNo);
			log.info("邮箱登录:"+accountNo);
		} 
		else if(accountNo.matches(ph))
		{
			//手机号登录
			entity.setMobile(accountNo);
			log.info("手机号登录:"+accountNo);
		}
		else
		{
			//就是用户名登录
			//user.setUsername(username);
			entity.setUserName(accountNo);
			log.info("用户名登录:"+accountNo);
		}
		entity.setUserPass(passWord);
		entity=userBiz.login(entity);
		if(null!=entity)
		{
			ssoUserEntity=new SSOUserEntity();
			ssoUserEntity.setAccount(accountNo);
			ssoUserEntity.setUserId(entity.getUserId());
			ssoUserEntity.setUserName(entity.getUserName());
			code="000000";
			msg="successful";
		}
		
		redult.setCode(code);
		redult.setMsg(msg);
		redult.setSsoUserEntity(ssoUserEntity);
		long end = System.currentTimeMillis();        
		log.info("<<===================执行耗时:"+(end-start)+"毫秒============================>>"); 
		return redult;
	}


	@Override
	public boolean saveTicket(Ticket ticket,String key) {
		boolean bl=false;
		TicketEntity ticketEntity=new TicketEntity();
		ticketEntity.setEnable(0);
		ticketEntity.setKey(key);
		ticketEntity.setCreateTime(ticket.getCreateTime()+"");
		ticketEntity.setTicket(ticket.toString());
		
		ticketBiz.save(ticketEntity);
		bl=true;
		return bl;
	}

	@Override
	public Ticket loadTicket(String key) {
		TicketEntity ticketEntity=ticketBiz.getById(key);
		Ticket ticket=new Ticket();
		if(null!=ticketEntity)
		{
			String st=ticketEntity.getTicket();
			ObjectMapper mapper = new ObjectMapper(); 
			try {
				ticket = mapper.readValue(st, Ticket.class);
				log.info("Ticket is::"+ticket.toString());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return ticket;
	}
	@Override
	public boolean deleteTicketByKey(String key) {
		boolean bl=false;

		
		ticketBiz.deleteById(key);;
		bl=true;
		return bl;
	}

	/***************ynet特别版*************/


}
