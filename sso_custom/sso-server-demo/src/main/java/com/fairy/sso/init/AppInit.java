/**
 *  版权所有 Copyright@2016年4月7日下午4:54:41
 */
package com.fairy.sso.init;

import javax.servlet.ServletContext;

import org.apache.log4j.Logger;
import org.springframework.web.context.ServletContextAware;

/**
 * [AppInit.java.java]
 *
 * @Author: [yyk1504@163.com]
 * @CreateDate: [2016年4月7日下午4:54:41]
 * @Update: [说明本次修改内容] by [yyk1504@163.com] [2016年4月7日下午4:54:41]
 * @Version: [v1.0]
 */
public class AppInit implements ServletContextAware{

	private Logger log= Logger.getLogger(AppInit.class);

	@Override
	public void setServletContext(ServletContext servletContext) {
		// TODO Auto-generated method stub
		log.info("................................................从配置文件初始化参数.......................................................");
	}

}
