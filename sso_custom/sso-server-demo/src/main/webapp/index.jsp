<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@page import="java.util.*"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<!DOCTYPE html>
<html>
<head>
<title>登录</title> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<script type="text/javascript" src="${pageContext.servletContext.contextPath }/plugin/login/js/jquery-1.9.0.min.js"></script>
<script type="text/javascript" src="${pageContext.servletContext.contextPath }/plugin/login/images/login.js"></script>
<link href="${pageContext.servletContext.contextPath }/plugin/login/css/login2.css" rel="stylesheet" type="text/css" />
</head>
<body>
<h1>fairy平台登录<sup>2016</sup></h1>

<div class="login" style="margin-top:50px;">
    
    <div class="header">
        <div class="switch" id="switch">
		    <a class="switch_btn_focus" id="switch_qlogin" href="javascript:void(0);" tabindex="7">快速登录</a>
			<!--
			<a class="switch_btn" id="switch_login" href="javascript:void(0);" tabindex="8">快速注册</a>
			-->
			<div class="switch_bottom" id="switch_bottom" style="position: absolute; width: 64px; left: 0px;"></div>
        </div>
    </div>    
  
    
    <div class="web_qr_login" id="web_qr_login" style="display: block; height: 295px;">    

            <!--登录-->
            <div class="web_login" id="web_login">
               
               
               <div class="login-box">
						<div class="login_form">
						    <div><font color="red" size="4">${tips }</font></div>
							<form action="login.do"  name="loginform" accept-charset="utf-8" id="login_form" class="loginForm" method="post">
								<input type="hidden" name="service" value="${service }"/>
								<input type="hidden" name="to" value="log"/>
								<div class="uinArea" id="uinArea">
									<label class="input-tips" for="u">帐号：</label>
									<div class="inputOuter" id="uArea">
										<input type="text" id="accountNo" name="accountNo" value="10000092"  class="inputstyle"/>
									</div>
								</div>
								<div class="pwdArea" id="pwdArea">
								   <label class="input-tips" for="p">密码：</label> 
								   <div class="inputOuter" id="pArea">
										
										<input type="password" id="passWord" name="passWord"  value="123" class="inputstyle"/>
									</div>
								</div>
			                    <div class="uinArea" id="uinArea">
									<label class="input-tips" for="u">验证码：</label>
									<div class="inputOuter" id="uArea">
										<input type="text" id="codeImg" name="codeImg" value="1234" class="inputstyle"   style="width:100px;"/>
										<img id="imgCheck" src="${pageContext.servletContext.contextPath }/img.do" class="codeImg" onclick="refreshCodes();"/>
									</div>
								</div>							   
								<div style="padding-left:50px;margin-top:20px;">
									<input type="button" value="登 录" style="width:150px;" class="button_blue" onclick="login()"/>
								</div>
			              </form>
			           </div>
           
            	</div>
               
            </div>
            <!--登录end-->
  </div>

  <!--注册-->
    <div class="qlogin" id="qlogin" style="display: none; ">
   
    <div class="web_login">
	<form name="form2" id="regUser" accept-charset="utf-8"  action="" method="post">
	      <input type="hidden" name="to" value="reg"/>
		      		       <input type="hidden" name="did" value="0"/>
        <ul class="reg_form" id="reg-ul">
        		<div id="userCue" class="cue">快速注册请注意格式</div>
                <li>
                	
                    <label for="user"  class="input-tips2">用户名：</label>
                    <div class="inputOuter2">
                        <input type="text" id="user" name="user" maxlength="16" class="inputstyle2"/>
                    </div>
                    
                </li>
                
                <li>
                <label for="passwd" class="input-tips2">密码：</label>
                    <div class="inputOuter2">
                        <input type="password" id="passwd"  name="passwd" maxlength="16" class="inputstyle2"/>
                    </div>
                    
                </li>
                <li>
                <label for="passwd2" class="input-tips2">确认密码：</label>
                    <div class="inputOuter2">
                        <input type="password" id="passwd2" name="" maxlength="16" class="inputstyle2" />
                    </div>
                    
                </li>
                
                <li>
                 <label for="qq" class="input-tips2">QQ：</label>
                    <div class="inputOuter2">
                       
                        <input type="text" id="qq" name="qq" maxlength="10" class="inputstyle2"/>
                    </div>
                   
                </li>
                
                <li>
                    <div class="inputArea">
                        <input type="button" id="reg"  style="margin-top:10px;margin-left:85px;" class="button_blue" value="同意协议并注册"/> <a href="#" class="zcxy" target="_blank">注册协议</a>
                    </div>
                    
                </li><div class="cl"></div>
            </ul></form>
           
    
    </div>
   
    
    </div>
    <!--注册end-->
</div>
<div class="jianyi">*推荐使用ie8或以上版本ie浏览器或Chrome内核浏览器访问本站</div>
<script type="text/javascript">
function login()
{
	var datas="userId=44444";
	document.getElementById('login_form').submit();
    /*
    var urls='/${pageContext.servletContext.contextPath }/login.do';
	$.ajax({ 
			type: "post", 
			url: urls, 
			async: true,  
			data: datas, 
			dataType: "text", 
			success: function(data) { 
			    //alert(data); 
			    var jsonobj=eval('('+data+')'); 
			    //alert(data);
			    //$("#userId").val(jsonobj.userId);
			    //document.getElementById('user_id_html').innerText=jsonobj.userId;
			   // document.getElementById('user_name_html').innerText=jsonobj.userName;
			    //document.getElementById('user_userBirthday_html').innerText=jsonobj.userBirthday;
			    //document.getElementById('user_userSalary_html').innerText=jsonobj.userSalary;
			    //$("#userName").val(jsonobj.userName);
			    //$("#userBirthday").val(jsonobj.userBirthday);
			    //$("#userSalary").val(jsonobj.userSalary);
			    document.location.href='index.html';
			}, 
			error: function(e) { 
				alert('error:'+e); 
			} 
		}); 
		*/	
}
/**替换图片**/
function refreshCodes(){
	//alert(1);
	//$("#imgCheck").src='';
	//$("#imgCheck").src = "${pageContext.servletContext.contextPath }/img.do"+Math.random();
	var verify=document.getElementById('imgCheck');
	verify.setAttribute('src','${pageContext.servletContext.contextPath }/img.do?'+Math.random());
}
</script>
</body>
</html>
